#!/bin/bash
inst_ids=`aws ec2 describe-instances --query 'Reservations[*].Instances[*].InstanceId' --output text --filters file://$AWSTOOLSSRCDIR/files/describe-instance-filters.json`
for inst_id in $inst_ids 
do
    echo "Terminating instance: $inst_id"
    read -p "Please approve: (y/n)" choice
    case "$choice" in
        y|Y)
            echo "terminating..."
            aws ec2 terminate-instances --instance-ids $inst_id
            ;;
        n|N)
            echo "skipping..."
            ;;
        *)
            echo "invalid input, abborting..."
            ;;
    esac

done
