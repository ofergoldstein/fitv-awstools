#!/bin/bash
aws ec2 describe-instances --query 'Reservations[].Instances[].PublicDnsName' --filters file://$AWSTOOLSSRCDIR/files/describe-instance-filters.json
