#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "No host supplied"
else
    echo "Looking for host: $1"
    HOST=$1
    awsuser=`cat ~/.ssh/awstools_config.json | jq --arg HOST $HOST '.Hosts[] | select(.Name | contains($HOST)) | .User'`
    awsinstance=`cat ~/.ssh/awstools_config.json | jq --arg HOST $HOST '.Hosts[] | select(.Name | contains($HOST)) | .InstanceType'`
    awsimageid=`cat ~/.ssh/awstools_config.json | jq --arg HOST $HOST '.Hosts[] | select(.Name | contains($HOST)) | .ImageId'`
    awssecgroup=`cat ~/.ssh/awstools_config.json | jq --arg HOST $HOST '.Hosts[] | select(.Name | contains($HOST)) | .SecurityGroupIds[]'`
    awskeyname=`cat ~/.ssh/awstools_config.json | jq --arg HOST $HOST '.Hosts[] | select(.Name | contains($HOST)) | .KeyName'`

    export AWSUSER=$awsuser
    export AWSINSTANCE=$awsinstance
    export AWSIMAGEID=$awsimageid
    export AWSSECGROUP=$awssecgroup
    export AWSKEYNAME=$awskeyname

    echo -e "AWSUSER: $AWSUSER\nAWSINSTANCE: $AWSINSTANCE\nAWSIMAGEID: $AWSIMAGEID\nAWSSECGROUP: $AWSSECGROUP\nAWSKEYNAME: $AWSKEYNAME"

    rm -rf $AWSTOOLSSRCDIR/files/spot-instance-tmp*
    echo -e "\nNew JSON file:"
    #cat $AWSTOOLSSRCDIR/files/spot-instance.json | jq --arg AWSIMAGEID $AWSIMAGEID --arg AWSINSTANCE $AWSINSTANCE --arg AWSSECGROUP $AWSSECGROUP --arg AWSKEYNAME $AWSKEYNAME '.ImageId=$AWSIMAGEID | .InstanceType=$AWSINSTANCE | .SecurityGroupIds=$AWSSECGROUP | .KeyName=$AWSKEYNAME' > $AWSTOOLSSRCDIR/files/spot-instance-tmp.json
    cat $AWSTOOLSSRCDIR/files/spot-instance.json | jq --arg AWSIMAGEID $AWSIMAGEID --arg AWSINSTANCE $AWSINSTANCE '.ImageId=$AWSIMAGEID | .InstanceType=$AWSINSTANCE' > $AWSTOOLSSRCDIR/files/spot-instance-tmp.json
    mv $AWSTOOLSSRCDIR/files/spot-instance-tmp.json $AWSTOOLSSRCDIR/files/spot-instance.json
    cat $AWSTOOLSSRCDIR/files/spot-instance.json | jq --arg AWSSECGROUP $AWSSECGROUP --arg AWSKEYNAME $AWSKEYNAME '.SecurityGroupIds[]=$AWSSECGROUP | .KeyName=$AWSKEYNAME' > $AWSTOOLSSRCDIR/files/spot-instance-tmp.json
    mv $AWSTOOLSSRCDIR/files/spot-instance-tmp.json $AWSTOOLSSRCDIR/files/spot-instance.json
    sed -i '' 's/\\"//g' $AWSTOOLSSRCDIR/files/spot-instance.json
    jq '.' $AWSTOOLSSRCDIR/files/spot-instance.json
fi

