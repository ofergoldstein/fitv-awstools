#!/bin/bash

# check if aws cli is installed
echo "# Checking aws cli version"
aws --version
if [ $? -eq 0 ]; then
    echo "aws cli is installed in this machine, continuing with setup";
else
    echo "aws cli is not installed in this machine, please install with:";
    echo "curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"";
    echo "sudo installer -pkg AWSCLIV2.pkg -target /";
fi

# add environment variables
echo "# Adding environment variables"
echo "##------ start fitv-awstools setup ------##" >> ~/.bash_profile
AWSTOOLSSRCDIR=`pwd`
echo "export AWSTOOLSSRCDIR="$AWSTOOLSSRCDIR"" >> ~/.bash_profile
AWSTOOLSUSERID=$(aws iam get-user --query 'User.Arn' | grep -o -E '[0-9]+')
echo "export AWSTOOLSUSERID=$AWSTOOLSUSERID" >> ~/.bash_profile
cp utils/.awstools_aliases ~/
echo "source ~/.awstools_aliases" >> ~/.bash_profile
echo "##------ end fitv-awstools setup ------##" >> ~/.bash_profile
cp files/awstools_config.json ~/.ssh/

rm -rf $AWSTOOLSSRDDIR/files/describe-instance-filters-tmp.json
cat $AWSTOOLSSRCDIR/files/describe-instance-filters.json | jq --arg AWSTOOLSUSERID $AWSTOOLSUSERID '.[1].Values=[$AWSTOOLSUSERID]' > $AWSTOOLSSRCDIR/files/describe-instance-filters-tmp.json
mv $AWSTOOLSSRCDIR/files/describe-instance-filters-tmp.json $AWSTOOLSSRCDIR/files/describe-instance-filters.json
