#!/bin/bash
inst_ids=`aws ec2 describe-instances --query 'Reservations[*].Instances[*].InstanceId' --output text --filters file://$AWSTOOLSSRCDIR/files/cancel-spot-request-filters.json`
for inst_id in $inst_ids 
do
    echo "Cancelling spot request: $inst_id"
        read -p "Please approve: (y/n)" choice
        case "$choice" in
            y|Y)
                echo "cancelling..."
                aws ec2 cancel-spot-requests --spot-instance-request-ids $inst_id
                ;;
            n|N)
                echo "skipping..."
                ;;
            *)
                echo "invalid input, abborting..."
                ;;
        esac

done
