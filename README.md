# fitV AWSTOOLS  
A simple tool-kit for aws services. 

## Table of contents
1. [Prerequisites](#Prerequisites)
2. [First time install](#First-time-install)
3. [Usage guide](#Usage)
4. [Common commands](#Common-commands)  


## Prerequisites  
* aws-cli
* configured aws user on your machine  


## First time install
Make sure aws cli is already installed and configured on your machine.  
Find install instructions [here](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html)  
Clone this repo into a destination of your choice and execute the following commands:  
  
  
```bash
cd <local-repo-parent-dir-path>
git clone https://ofergoldstein@bitbucket.org/ofergoldstein/fitv-awstools.git
cd fitv-awstools  
./setup.sh
```

## Common commands
**awsspotrequest** request a spot instance  
**awsssh** ssh into a running instance  
**awsterminst** terminate a running instance (requires approval) 
**awskeys** print out all available keys  
**awsconf <hostname\>** configure request to relevant ami 
**awshosts** print out all available hosts  
**awsdns** print out all my running instances  

## Usage guide
After completeing the install process-  
+ Copy all your keys (.pem) to `fitv-awstools/keys/`  
+ Check your available hosts with `awshosts`  
+ If you wish to add hosts just add another block to ~/.ssh/awstools_config.json with the relevant field values  
+ Copy the hostname you wish to ramp up and configure the run with `awsconf <hostname>`  
+ Ramp up an instance with spot request `awsspotrequest`  
+ Ssh into the instance `awsssh`  
+ ones you finished - terminate the run `awsterminst`  


