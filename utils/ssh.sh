#!/bin/bash
key=${AWSKEYNAME//\"/}
user=${AWSUSER//\"/}
PRIVATE_KEY="$AWSTOOLSSRCDIR/keys/$key.pem"
echo "Connection information:"
line="User: $user"
echo $line
line="Key: $PRIVATE_KEY"
echo $line

AWS=`aws ec2 describe-instances --query 'Reservations[*].Instances[*].PublicDnsName' --output text --filters file://$AWSTOOLSSRCDIR/files/describe-instance-filters.json`
tmparr=($AWS)
len=${#AWS[@]}
#echo $len
if [ $len -lt 1 ]; then
    echo "ssh-ing into: ${tmparr[1]}"
    #ssh -i $PRIVATE_KEY -L 8157:127.0.0.1:8888 ec2-user@$AWS
    ssh -i $PRIVATE_KEY -L localhost:8888:localhost:8888 $user@${tmparr[1]}
else
    echo "ssh-ing into: ${tmparr[$len-1]}"
    ssh -i $PRIVATE_KEY -L localhost:8889:localhost:8889 $user@${tmparr[$len-1]}
fi
